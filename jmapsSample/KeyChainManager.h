//
//  KeyChainManager.h
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyChainInformation.h"

@interface KeyChainManager : NSObject

+(KeyChainManager*) sharedManager;

@property(nonatomic,strong) KeyChainInformation* information;

@end
