//
//  AppDelegate.h
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
