//
//  IncredistViewController.m
//  jmapsSample
//
//  Created by flight on 2014/01/21.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "IncredistViewController.h"
#import "IncredistManager.h"

@interface IncredistViewController ()

@property(nonatomic,weak) IBOutlet UITextView* textView;
@property(nonatomic,weak) IBOutlet UIButton*   button;
@property(nonatomic,weak) IBOutlet UIButton*   emvButton;
@end

@implementation IncredistViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _button.enabled = YES;
    _emvButton.enabled = NO;
    _textView.text = @"Incredistには接続されていません。";
    [_button setTitle:@"Incredistの接続をしてみる" forState:UIControlStateNormal];
     _button.tag = 1;
    [_emvButton setTitle:@"EMVコマンドは使用できんぜよ" forState:UIControlStateNormal];
    _emvButton.tag = 1;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [[IncredistManager sharedManager] disConnectDevive];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pushedButton:(id)sender{
    _button.enabled = NO;
    _emvButton.enabled = NO;
    [_button setTitleColor:[UIColor redColor] forState:UIControlStateDisabled];
    NSString* savedTitle = [_button titleForState:UIControlStateNormal];
    __block IncredistViewController* blockSelf = self;
    IncredistManager* incredistManager = [IncredistManager sharedManager];
    incredistManager.completedResponseHandler = ^(FSIncredistLib* device,BOOL connected){
        blockSelf.button.enabled = YES;
        if (connected) {
            blockSelf.textView.text = [NSString stringWithFormat:@"接続が完了しました。\nBlueToothAddress = %@",device.readBluetoothAddressToDevice];
            blockSelf.button.tag = 2;
            [blockSelf.button setTitle:@"接続を解除" forState:UIControlStateNormal];
            blockSelf.emvButton.enabled = YES;
            blockSelf.emvButton.tag = 1;
            [blockSelf.emvButton setTitle:@"EMV OPEN" forState:UIControlStateNormal];
        } else {
            blockSelf.textView.text = @"接続を解除しました。";
            blockSelf.button.tag = 1;
            [blockSelf.button setTitle:@"Incredistの接続をしてみる" forState:UIControlStateNormal];
            blockSelf.emvButton.enabled = NO;
            
            [blockSelf.emvButton setTitle:@"EMVコマンドは使用できんぜよ" forState:UIControlStateNormal];
        }
    };
    incredistManager.responseErrorHandler = ^(NSString* errorMessage){
        blockSelf.button.enabled = YES;
        blockSelf.textView.text = errorMessage;
        [blockSelf.button setTitle:savedTitle forState:UIControlStateNormal];
    };
    if (_button.tag == 1) {
        [_button setTitle:@"接続中..." forState:UIControlStateDisabled];
        [incredistManager connectDevice];
    } else if (_button.tag == 2){
        [_button setTitle:@"解除中..." forState:UIControlStateDisabled];
        [incredistManager disConnectDevive];
    }
}

-(IBAction)pushedEmvButton:(id)sender{
    _emvButton.enabled = NO;
    _button.enabled = NO;
    [_emvButton setTitleColor:[UIColor redColor] forState:UIControlStateDisabled];
    NSString* savedTitle = [_emvButton titleForState:UIControlStateNormal];
    __block IncredistViewController* blockSelf = self;
    IncredistManager* incredistManager = [IncredistManager sharedManager];
    incredistManager.completedEMVCommandHandler = ^(NSData* metaData,NSData* sw1Data,NSData* sw2Data){
        blockSelf.button.enabled = YES;
        blockSelf.emvButton.enabled = YES;
        [blockSelf EMVResponsedMetaData:metaData sw1Data:sw1Data sw2Data:sw2Data];
    };
    incredistManager.responseErrorHandler = ^(NSString* errorMessage){
        blockSelf.button.enabled = YES;
        blockSelf.emvButton.enabled = YES;
        blockSelf.textView.text = errorMessage;
        [blockSelf.button setTitle:savedTitle forState:UIControlStateNormal];
    };
    if (_emvButton.tag == 1) {
        [_emvButton setTitle:@"EMV OPEN NOW..." forState:UIControlStateDisabled];
        [incredistManager emvOpen];
    } else if (_emvButton.tag == 2) {
        [_emvButton setTitle:@"SELECTコマンド送信..." forState:UIControlStateDisabled];
        NSString* pseFileName = @"1PAY.SYS.DDF02";
        NSData* pseFileNameData = [pseFileName dataUsingEncoding:NSUTF8StringEncoding];
        [incredistManager selectFileName:pseFileNameData];
    } else if (_emvButton.tag == 3) {
        [_emvButton setTitle:@"AIDコマンド送信..." forState:UIControlStateDisabled];
        [self sendSelectAID];
    } else if (_emvButton.tag == 4) {
        [_emvButton setTitle:@"READ RECORDコマンド送信..." forState:UIControlStateDisabled];
        //        //コマンド
        [incredistManager readRecordByNumber:1 sfiNumber:2];
    }  else if (_emvButton.tag == 5) {
        [_emvButton setTitle:@"EMV ATR..." forState:UIControlStateDisabled];
        [incredistManager answerToReset];
    }else if (_emvButton.tag == 6) {
        [_emvButton setTitle:@"EMV CLOSE NOW..." forState:UIControlStateDisabled];
        [incredistManager emvClose];
    }
}

-(void) sendSelectAID{
    if (![[IncredistManager sharedManager] nextSelectAID]) {
        [self setEmvCloseStatus];
    }
}

-(void) setEmvCloseStatus{
    _emvButton.tag = 6;
    [_emvButton setTitle:@"EMV CLOSE" forState:UIControlStateNormal];
}

-(BOOL) isSuccessfullStatusBySW1:(NSString*) sw1 SW2:(NSString*) sw2{
    return ([sw1 isEqualToString:@"90"] && [sw2 isEqualToString:@"00"]);
}

-(void) EMVResponsedMetaData:(NSData*) data
                     sw1Data:(NSData*) sw1Data
                     sw2Data:(NSData*) sw2Data{
    
    NSString* sw1Str = [EMVDataUtils stringValueWithData:sw1Data];
    NSString* sw2Str = [EMVDataUtils stringValueWithData:sw2Data];
    _textView.text = [NSString stringWithFormat:@"sw1 = %@\nsw2 = %@\ndata = %@",sw1Str,sw2Str,[EMVDataUtils stringValueWithData:data]];
    IncredistManager* incredistManager = [IncredistManager sharedManager];
    if (_emvButton.tag == 1) {
        if ([self isSuccessfullStatusBySW1:sw1Str SW2:sw2Str]) {
            _emvButton.tag = 2;
            [_emvButton setTitle:@"SELECT COMMAND PSE FILE" forState:UIControlStateNormal];
        } else {
            [self setEmvCloseStatus];
        }
    } else if (_emvButton.tag == 2) {
        if ([self isSuccessfullStatusBySW1:sw1Str SW2:sw2Str] ||
            ([sw1Str isEqualToString:@"62"] && [sw2Str isEqualToString:@"83"])) {
            _emvButton.tag = 4;
            [_emvButton setTitle:@"EMV READ RECORD" forState:UIControlStateNormal];
        } else if ([sw1Str isEqualToString:@"6A"] && [sw2Str isEqualToString:@"81"]) {
            [self setEmvCloseStatus];
        } else {
             _emvButton.tag = 3;
            [_emvButton setTitle:@"EMV AIDコマンド" forState:UIControlStateNormal];
        }
       
    } else if (_emvButton.tag == 3){
        if ([self isSuccessfullStatusBySW1:sw1Str SW2:sw2Str] && data){
            [incredistManager resetAIDList];
            NSDictionary* iccBinaryList = incredistManager.readIccBinaryList;
            NSData* resultData;
            data = [EMVDataUtils searchTagData:iccBinaryList.FCITempleteTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"FCITempData = %@",resultData.description);
            data = [EMVDataUtils searchTagData:iccBinaryList.dFNameTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"dfNameData = %@",resultData.description);
            data = [EMVDataUtils searchTagData:iccBinaryList.FCIPropertyTempleteTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"FCIPropertyTempleteData = %@",resultData.description);
            _emvButton.tag = 4;
            [_emvButton setTitle:@"EMV READ RECORD" forState:UIControlStateNormal];
        } else {
            [self sendSelectAID];
        }
    } else if(_emvButton.tag == 4){
        if ([self isSuccessfullStatusBySW1:sw1Str SW2:sw2Str] && data){
            NSDictionary* iccBinaryList = incredistManager.readIccBinaryList;
            NSData* resultData;
            data = [EMVDataUtils searchTagData:iccBinaryList.EMVProprietaryTemplateTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"EMVProprietaryTemplateTag = %@",resultData.description);
            data = [EMVDataUtils searchTagData:iccBinaryList.ADFNameTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"ADFNameData = %@",resultData.description);
            data = [EMVDataUtils searchTagData:iccBinaryList.applicationLabelTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"applicationLabelData = %@",resultData.description);
            data = [EMVDataUtils searchTagData:iccBinaryList.applicationPriorityIndicatorTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"ApplicationPrioryTyIndicatorData = %@",resultData.description);
            data = [EMVDataUtils searchTagData:iccBinaryList.directoryDiscretionaryTempleteTag
                                      withData:data
                                    resultData:&resultData];
            NSLog(@"directoryDiscretionaryTemplete = %@",resultData.description);
        }
        _emvButton.tag = 5;
        [_emvButton setTitle:@"EMV ATR" forState:UIControlStateNormal];
    } else if(_emvButton.tag == 5){
        [self setEmvCloseStatus];
    } else if(_emvButton.tag == 6){
        _emvButton.tag = 1;
        [_emvButton setTitle:@"EMV OPEN" forState:UIControlStateNormal];
    }
}

@end
