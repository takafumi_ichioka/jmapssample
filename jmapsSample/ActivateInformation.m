//
//  ActivateInformation.m
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "ActivateInformation.h"

@implementation ActivateInformation

+(NSString*) subscribeSequence{
    return [self informationList][@"SubscribeSequence"];
}

+(NSString*) activateId{
    return [self informationList][@"ActivateId"];
}

+(NSString*) oneTimePassword{
    return [self informationList][@"OneTimePassword"];
}

+(NSDictionary*) informationList{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"ActivateInformation" ofType:@"plist"];
    return [NSDictionary dictionaryWithContentsOfFile:filePath];
}

@end
