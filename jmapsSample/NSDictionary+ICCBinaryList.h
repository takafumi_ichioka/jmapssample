//
//  NSDictionary+ICCBinaryList.h
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ICCBinaryList)

+(instancetype) dictionaryWithICCBainaryList;

@property(nonatomic,readonly) NSData* endCommandValue;
@property(nonatomic,readonly) NSData* selectCommand;
@property(nonatomic,readonly) NSData* readRecordCommand;
@property(nonatomic,readonly) NSArray* cardCompanyAIDList;
@property(nonatomic,readonly) NSData* selectParam;
@property(nonatomic,readonly) NSData* FCITempleteTag;
@property(nonatomic,readonly) NSData* dFNameTag;
@property(nonatomic,readonly) NSData* FCIPropertyTempleteTag;
@property(nonatomic,readonly) NSData* ADFNameTag;
@property(nonatomic,readonly) NSData* applicationLabelTag;
@property(nonatomic,readonly) NSData* applicationPriorityIndicatorTag;
@property(nonatomic,readonly) NSData* directoryDiscretionaryTempleteTag;
@property(nonatomic,readonly) NSData* EMVProprietaryTemplateTag;

@end
