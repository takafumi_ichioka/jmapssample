//
//  HttpRequest.m
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "HttpRequest.h"

@interface HttpRequest ()

@property(nonatomic,strong) NSOperationQueue* queue;
@property(nonatomic,strong) NSMutableData*    responseData;
@property(nonatomic,copy)   CompletedHandler  handler;
@property(nonatomic,strong) NSURLRequest*      request;

@end

@implementation HttpRequest
{
    BOOL _isExecuting;
    BOOL _isFinished;
}

-(id) init{
    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
        _responseData = [[NSMutableData alloc] init];
    }
    return self;
}

-(void) dealloc{
    _queue = nil;
    _responseData = nil;
    _handler = nil;
    _request = nil;
}

-(void) startRequest:(NSURLRequest*) request completedHandler:(CompletedHandler) completedHandler{
    __block HttpRequest* blockSelf = self;
    dispatch_async([self getDispatchQueue], ^{
        blockSelf.request = request;
        blockSelf.handler = completedHandler;
        [blockSelf.queue addOperation:blockSelf];
    });
}

-(BOOL) isExecuting{
    return _isExecuting;
}

-(BOOL) isFinished{
    return _isFinished;
}

-(dispatch_queue_t) getDispatchQueue{
    return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}

-(void) start{
    [self setValue:@(YES) forKey:@"isExecuting"];
    @autoreleasepool {
        NSURLConnection* connection = [NSURLConnection connectionWithRequest:_request delegate:self];
        if (connection) {
            do {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:  0.2]];
            } while (_isExecuting);
        } else {
            [self finish];
        }
    }
}

-(void) finish{
    [self setValue:@(NO) forKey:@"isExecuting"];
    [self setValue:@(YES) forKey:@"isFinished"];
}

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    _responseData.length = 0;
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_responseData appendData:data];
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection{
    __block HttpRequest* blockSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (blockSelf.handler) {
            NSLog(@"recived data length = %@",blockSelf.responseData);
            blockSelf.handler(blockSelf.responseData,nil);
        }
        [blockSelf finish];
    });
    
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    __block HttpRequest* blockSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        blockSelf.responseData = nil;
        if (blockSelf.handler) {
            NSLog(@"error = %@",error.localizedDescription);
            blockSelf.handler(nil,error);
        }
        [blockSelf finish];
    });
}

-(BOOL) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace{
    return YES;
}

-(void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if (challenge.previousFailureCount < 5) {
        SecTrustRef trust = challenge.protectionSpace.serverTrust;
        SecTrustResultType resultType;
        OSStatus err = SecTrustEvaluate(trust, &resultType);
        BOOL trusted = NO;
        BOOL ignored = YES;
        if (err == noErr) {
            NSLog(@"noError:resultType = %d",resultType);
            if (resultType == kSecTrustResultProceed ||
                resultType == kSecTrustResultUnspecified) {
                trusted = YES;
            }
        }
        if (_isCertificate && !trusted) {
//            CertificationManager* certificationManager = [CertificationManager sharedManager];
//            if (certificationManager.anchors.count > 0) {
//                ignored = NO;
//                err = SecTrustSetAnchorCertificates(trust, (__bridge CFArrayRef)certificationManager.anchors);
//                if (err == noErr) {
//                    err = SecTrustSetAnchorCertificatesOnly(trust, NO);
//                    if (err == noErr) {
//                        SecTrustResultType resultTypeAtAnchor;
//                        err = SecTrustEvaluate(trust, &resultTypeAtAnchor);
//                        if (err == noErr) {
//                            NSLog(@"noError:resultTypeAtAnchor = %d",resultTypeAtAnchor);
//                            if (resultTypeAtAnchor == kSecTrustResultProceed ||
//                                resultTypeAtAnchor == kSecTrustResultUnspecified) {
//                                trusted = YES;
//                            }
//                        }
//                    }
//                }
//            }
        }
        NSLog(@"trusted = %d",trusted);
        if (trusted || ignored) {
            NSURLCredential* credential = [NSURLCredential credentialForTrust:trust];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
        } else {
            [[challenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
        }
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
}



@end
