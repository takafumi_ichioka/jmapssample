//
//  KeyChainManager.m
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "KeyChainManager.h"

#define kSavedInformationKeyChainKeyName @"savedInformationKeyChainKeyName"
#define kSavedInformationUserDefaultKeyName @"savedInformationUserDefaultKeyName"

static KeyChainManager* _sharedInstance;

@implementation KeyChainManager

@synthesize information = _information;

+(KeyChainManager*) sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[KeyChainManager alloc] init];
    });
    return _sharedInstance;
}

-(void) setInformation:(KeyChainInformation *)information{
    _information = information;
    [self savedKeyChainInformation:information];
}

-(KeyChainInformation*) information{
    if (!_information) {
        KeyChainInformation* information = [self loadedKeyChainInformation];
        if (!information) {
            information = [[KeyChainInformation alloc] init];
            NSString* uuidString = [UIDevice currentDevice].identifierForVendor.UUIDString;
            uuidString = [uuidString stringByReplacingOccurrencesOfString:@"-" withString:@""];
            information.termPrimaryNo = uuidString;
            [self savedKeyChainInformation:information];
        }
        _information = information;
    }
    return _information;
}

-(void) savedKeyChainInformation:(KeyChainInformation*) information{
    if (!information) {
        return;
    }
    NSDictionary* keyChainQuery = [self keyChainQuery];
    NSData* newData = [NSKeyedArchiver archivedDataWithRootObject:information];
    NSData* savedData = [self savedKeyChainDataAtKeyChainQuery:keyChainQuery];
    NSMutableDictionary* newItem = @{}.mutableCopy;
    newItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    newItem[(__bridge id)kSecAttrService] = kSavedInformationKeyChainKeyName;
    newItem[(__bridge id)kSecValueData] = newData;
    if (savedData) {
        SecItemUpdate((__bridge CFDictionaryRef)keyChainQuery,(__bridge CFDictionaryRef)newItem);
    } else {
        SecItemAdd((__bridge CFDictionaryRef)newItem, nil);
    }
    NSUserDefaults* userDefaluts = [NSUserDefaults standardUserDefaults];
    [userDefaluts setObject:newData forKey:kSavedInformationUserDefaultKeyName];
    [userDefaluts synchronize];
}

-(KeyChainInformation*) loadedKeyChainInformation{
    NSData* data = nil;
    NSDictionary* keyChainQuery = [self keyChainQuery];
    data = [self savedKeyChainDataAtKeyChainQuery:keyChainQuery];
    if (!data) {
        data = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedInformationUserDefaultKeyName];
        if (!data) {
            return nil;
        }
    }
    KeyChainInformation* information = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return information;
}

-(NSDictionary*) keyChainQuery{
    NSMutableDictionary* keyChainQuery = @{}.mutableCopy;
    keyChainQuery[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keyChainQuery[(__bridge id)kSecAttrService] = kSavedInformationKeyChainKeyName;
    keyChainQuery[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    keyChainQuery[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    return keyChainQuery;
}

-(NSData*) savedKeyChainDataAtKeyChainQuery:(NSDictionary*) keyChainQuery{
    NSData* data = nil;
    CFTypeRef ref;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, &ref);
    if (res == noErr) {
        NSMutableDictionary* readItem = ((__bridge NSDictionary*)ref).mutableCopy;
        readItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        readItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
        CFTypeRef pass;
        res = SecItemCopyMatching((__bridge CFDictionaryRef)readItem, &pass);
        if (res == noErr) {
            data = (__bridge NSData*)pass;
        }
        return nil;
    }
    return data;
}

-(NSString*) termPrimaryNo{
    NSMutableDictionary* keyChainQuery = @{}.mutableCopy;
    keyChainQuery[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keyChainQuery[(__bridge id)kSecAttrService] = @"UUIDTest";
    keyChainQuery[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    keyChainQuery[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFTypeRef ref;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, &ref);
    NSString* termPrimaryNo = @"";
    if (res == noErr) {
        NSMutableDictionary* readItem = ((__bridge NSDictionary*)ref).mutableCopy;
        readItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        readItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
        CFTypeRef pass;
        res = SecItemCopyMatching((__bridge CFDictionaryRef)readItem, &pass);
        if (res == noErr) {
            NSData* data = (__bridge NSData*)pass;
            termPrimaryNo = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
    } else {
        termPrimaryNo = [UIDevice currentDevice].identifierForVendor.UUIDString;
        termPrimaryNo = [termPrimaryNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSMutableDictionary* addItem = @{}.mutableCopy;
        addItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        addItem[(__bridge id)kSecAttrService] = @"UUIDTest";
        addItem[(__bridge id)kSecValueData] = [termPrimaryNo dataUsingEncoding:NSUTF8StringEncoding];
        
        SecItemAdd((__bridge CFDictionaryRef)addItem, nil);
    }
    NSLog(@"termPrimaryNo = %@ length = %d",termPrimaryNo,termPrimaryNo.length);
    return termPrimaryNo;
}

-(void) addKeyChainInformation{
    
}

-(NSData*) keyChainDataForKey:(NSString*) key{
    if (!key) {
        return nil;
    }
    NSData* data = nil;
    NSMutableDictionary* keyChainQuery = @{}.mutableCopy;
    keyChainQuery[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keyChainQuery[(__bridge id)kSecAttrService] = key;
    keyChainQuery[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    keyChainQuery[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFTypeRef ref;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, &ref);
    if (res == noErr) {
        NSMutableDictionary* readItem = ((__bridge NSDictionary*)ref).mutableCopy;
        readItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        readItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
        CFTypeRef pass;
        res = SecItemCopyMatching((__bridge CFDictionaryRef)readItem, &pass);
        if (res == noErr) {
            data = (__bridge NSData*)pass;
        }
    }
    return data;
}

@end
