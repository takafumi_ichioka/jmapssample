//
//  HttpRequestManager.m
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "HttpRequestManager.h"
#import "HttpRequest.h"
#import "KeyChainManager.h"
#import "ActivateInformation.h"

static HttpRequestManager* _sharedInstance;

@interface HttpRequestManager ()

@end

@implementation HttpRequestManager

+(HttpRequestManager*) sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[HttpRequestManager alloc] init];
    });
    return _sharedInstance;
}

-(KeyChainInformation*) keyChainInformation{
    if (!_keyChainInformation) {
        _keyChainInformation = [KeyChainManager sharedManager].information;
    }
    return _keyChainInformation;
}

-(void) testRequest{
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.yahoo.co.jp/"]];
    [self sendAsyncRequest:request
             isCertificate:NO];
}

-(void) getActivate{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"op0619.do?oP0619_1=aaa"];
    NSURL* URL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"subscribeSequence":[ActivateInformation subscribeSequence],
                             @"activateID":[ActivateInformation activateId],
                             @"oneTimePassword":[ActivateInformation oneTimePassword]};
    [self sendAsyncRequestURL:URL
                     atParams:params
                isCertificate:NO];
    
}

-(void) downloadCertificateData{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"op0619.do?oP0619_3=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"subscribeSequence":[ActivateInformation subscribeSequence],
                             @"activateID":[ActivateInformation activateId],
                             @"oneTimePassword":[ActivateInformation oneTimePassword],
                             @"termPrimaryNo":_keyChainInformation.termPrimaryNo};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}
-(void) sendResultActivate{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"op0619.do?oP0619_2=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"uniqueId":_keyChainInformation.uniqueId};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) open{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"opzz.do?oPZZ72_2=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"uniqueId":_keyChainInformation.uniqueId,@"printResult":@(NO)};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) close{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"opzz.do?oPZZ73=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"uniqueId":_keyChainInformation.uniqueId};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) stopService{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"mA0127.do?mA0127=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) getCreditDayInformation{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"dt0127.do?dT0127_1=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isRePrint":@"false",@"detailOption":@(1),@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) getCreditDayResult{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"dt0127.do?dT0127_2=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"outoutDate":@"2014-01-20 16:45:35.792",@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) getCreditJournal{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"pt0127.do?pT0127=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isRePrint":@"false",@"detailOption":@(1),@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) reprintForCredit{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"rp0134.do?rP0134=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isRePrint":@"false",
                             @"detailOption":@(1),
                             @"journalDiv":@(1),
                             @"isTraining":@"true",
                             @"printResult":@"false"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) cardCompanyList{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"pp0701.do?pp0701=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) informationList{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"if0201.do?if0201=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"orderBy":@"Descending",@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) downloadLogoImage{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"ma9301.do?mA9301=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) termServiceSetting{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"pp1101.do?pp1101=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"isTraining":@"true"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) creditTradeStart{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"mA0447.do?mA0447=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"uniqueId":_keyChainInformation.uniqueId,
                             @"serviceDiv":@(0),
                             @"isTraining":@"true",
                             @"printResult":@"false",};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) creditReading{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"mA9271.do?mA9271=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSData* cryptData = [@"0384095" dataUsingEncoding:NSUTF8StringEncoding];
    NSString* cryptDataStr = cryptData.description;
    cryptDataStr = [cryptDataStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    cryptDataStr = [cryptDataStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    cryptDataStr = [cryptDataStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSDictionary* params = @{@"keySerialNo":@"55555555555555555555",
                             @"cryptData":cryptDataStr,
                             @"settlementMediaDiv":@(0),
                             @"isTraining":@"true",
                             @"operationDiv":@(0),
                             @"isMSFallback":@"false",
                             @"isFallback":@"false"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) initAppliWithAID:(NSString*) aid andFCI:(NSString*) fci{
    NSString* targetURL = [[self baseURLCertificated:NO] stringByAppendingPathComponent:@"ma770.do?mA0770_1=aaa"];
    NSURL* baseURL = [NSURL URLWithString:targetURL];
    NSDictionary* params = @{@"fci":fci,
                             @"aid":aid,
                             @"brandApVersion":@"0020",
                             @"brandApJudgeNo":@"00",
                             @"ifdSerialNumber":@"55555555"};
    [self sendAsyncRequestURL:baseURL
                     atParams:params
                isCertificate:NO];
}

-(void) sendAsyncRequestURL:(NSURL*) URL
                   atParams:(NSDictionary*) params
              isCertificate:(BOOL) isCertificate{
    NSLog(@"URL = %@",URL);
    NSLog(@"params = %@",params);
    if (!params) {
        params = @{};
    }
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSLog(@"jsonData = %@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:30];
    [request setHTTPBody:jsonData];
    if (_keyChainInformation.uniqueId) {
        NSLog(@"uniqueId = %@",_keyChainInformation.uniqueId);
        [request setValue:_keyChainInformation.uniqueId forHTTPHeaderField:@"uniqueId"];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self sendAsyncRequest:request isCertificate:isCertificate];
}

-(NSString*) baseURLCertificated:(BOOL) certificated{
    NSString* baseURLKey = (certificated) ? @"APIURLAtClientCerFile":@"APIURLAtNotClientCerFile";
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:baseURLKey];
}

-(void) sendAsyncRequest:(NSURLRequest*) request isCertificate:(BOOL) isCertificate{
    HttpRequest* httpRequest = [[HttpRequest alloc] init];
    httpRequest.isCertificate = isCertificate;
    __block HttpRequestManager* blockSelf = self;
    [httpRequest startRequest:request completedHandler:^(NSData* data,NSError* error){
        [blockSelf completedNotificationData:data failedError:error];
    }];
    
}

-(void) completedNotificationData:(NSData*) data failedError:(NSError*) error{
    NSMutableDictionary* userInfo = @{}.mutableCopy;
    if (data) {
        userInfo[kHttpRequestCompletedDataUserInfoName] = data;
    }
    if (error) {
        userInfo[kHttpRequestCompletedErrorUserInfoName] = error;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kHttpRequestCompletedNotificationName
                                                        object:self
                                                      userInfo:userInfo];
}



@end
