//
//  HttpRequest.h
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CompletedHandler)(NSData* data,NSError* error);

@interface HttpRequest : NSOperation<NSURLConnectionDataDelegate,NSURLConnectionDelegate>

@property(nonatomic) BOOL isCertificate;

-(void) startRequest:(NSURLRequest*) request completedHandler:(CompletedHandler) completedHandler;

@end
