//
//  NSDictionary+ICCBinaryList.m
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "NSDictionary+ICCBinaryList.h"

NSDictionary* _iccBinaryListInstance;

@implementation NSDictionary (ICCBinaryList)

+(instancetype) dictionaryWithICCBainaryList{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* path = [[NSBundle mainBundle] pathForResource:@"ICCBinaryData" ofType:@"plist"];
        _iccBinaryListInstance = [self dictionaryWithContentsOfFile:path];
    });
    return _iccBinaryListInstance;
}

-(NSData*) endCommandValue{
    return self[@"EndCommandValue"];
}

-(NSData*) selectCommand{
    return [self emvCommandForKey:@"Select"];
}

-(NSData*) readRecordCommand{
    return [self emvCommandForKey:@"ReadRecord"];
}

-(NSArray*) cardCompanyAIDList{
    return self[@"CardCompanyAIDList"];
}

-(NSData*) selectParam{
    return [self emvCommandParamForKey:@"Select"];
}

-(NSData*) FCITempleteTag{
    return [self emvTagForKey:@"FCITemplate"];
}

-(NSData*) dFNameTag{
    return [self emvTagForKey:@"DFName"];
}

-(NSData*) FCIPropertyTempleteTag{
    return [self emvTagForKey:@"FCIPropertyTemplate"];
}

-(NSData*) ADFNameTag{
    return [self emvTagForKey:@"ADFName"];
}

-(NSData*) applicationLabelTag{
    return [self emvTagForKey:@"ApplicationLabel"];
}

-(NSData*) applicationPriorityIndicatorTag{
    return [self emvTagForKey:@"ApplicationPriorityIndicator"];
}

-(NSData*) directoryDiscretionaryTempleteTag{
    return [self emvTagForKey:@"DirectoryDiscretionaryTemplete"];
}

-(NSData*) EMVProprietaryTemplateTag{
    return [self emvTagForKey:@"EMVProprietaryTemplate"];
}

-(NSData*) emvCommandForKey:(NSString*) key{
    return self[@"EMVCommandList"][key];
}

-(NSData*) emvCommandParamForKey:(NSString*) key{
    return self[@"EMVCommandParamList"][key];
}

-(NSData*) emvTagForKey:(NSString*) key{
    return self[@"EMVTagList"][key];
}

@end
