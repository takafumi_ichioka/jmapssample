//
//  HttpRequestManager.h
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyChainInformation.h"

static NSString* kHttpRequestCompletedNotificationName = @"kHttpRequestCompletedNotificationName";
static NSString* kHttpRequestCompletedDataUserInfoName = @"kHttpRequestCompletedDataUserInfoName";
static NSString* kHttpRequestCompletedErrorUserInfoName = @"kHttpRequestCompletedErrorUserInfoName";

@interface HttpRequestManager : NSObject

+(HttpRequestManager*) sharedManager;

@property(nonatomic,strong) KeyChainInformation* keyChainInformation;

-(void) testRequest;

-(void) getActivate;

-(void) downloadCertificateData;

-(void) sendResultActivate;

-(void) open;

-(void) close;

-(void) stopService;

-(void) getCreditDayInformation;

-(void) getCreditDayResult;

-(void) getCreditJournal;

-(void) reprintForCredit;

-(void) creditTradeStart;

-(void) creditReading;

-(void) cardCompanyList;

-(void) termServiceSetting;

-(void) informationList;

-(void) downloadLogoImage;

-(void) initAppliWithAID:(NSString*) aid andFCI:(NSString*) fci;

@end
