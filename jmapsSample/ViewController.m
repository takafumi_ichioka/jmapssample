//
//  ViewController.m
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "ViewController.h"
#import "HttpRequestManager.h"
#import "KeyChainManager.h"

@interface ViewController ()

@property(nonatomic,weak) IBOutlet UITextView* textView;
@property(nonatomic,weak) IBOutlet UIButton*   button;
@property(nonatomic,weak) IBOutlet UIButton*   buttonCredit;
@property(nonatomic,weak) IBOutlet UIActivityIndicatorView* indicator;
@property(nonatomic,strong) KeyChainInformation* keyChainInformation;
@property(nonatomic)       BOOL isCreditExcute;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    [_button setTitle:@"アクティベート情報取得" forState:UIControlStateNormal];
//    _keyChainInformation = [KeyChainManager sharedManager].information;
//    [HttpRequestManager sharedManager].keyChainInformation = _keyChainInformation;
    [_button setTitle:@"ロゴ" forState:UIControlStateNormal];
    _button.tag = 5;
    [_buttonCredit setTitle:@"クレジット取引" forState:UIControlStateNormal];
    _buttonCredit.tag = 0;
    _keyChainInformation = [[KeyChainInformation alloc] init];
    _keyChainInformation.uniqueId = @"JRM020000K000329";
    [HttpRequestManager sharedManager].keyChainInformation = _keyChainInformation;
    _indicator.hidden = YES;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(completedHttpRequestNotification:)
                                                 name:kHttpRequestCompletedNotificationName
                                               object:nil];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kHttpRequestCompletedNotificationName
                                                  object:nil];
}

-(NSDictionary*) responsedData:(NSData*) data reslutObjectForKey:(NSString*) key{
    NSDictionary* result = [self responseResultData:data];
    NSLog(@"result = %@",result);
    NSDictionary* obj = result[key];
    NSLog(@"obj = %@",obj);
    return obj;
}



-(NSDictionary*) responseResultData:(NSData*) data{
    return [NSJSONSerialization JSONObjectWithData:data
                                           options:NSJSONReadingAllowFragments
                                             error:nil];
}

-(IBAction)pushedButton:(id)sender{
    _indicator.hidden = NO;
    [_indicator startAnimating];
    _isCreditExcute = NO;
    _button.enabled = NO;
    _buttonCredit.enabled = NO;
    HttpRequestManager* httpManager = [HttpRequestManager sharedManager];
    if (_button.tag == 0) {
        [httpManager getActivate];
    } else if (_button.tag == 1){
        [httpManager downloadCertificateData];
    } else if (_button.tag == 2){
        [httpManager sendResultActivate];
        //[httpManager testRequest];
    } else if (_button.tag == 3){
        [httpManager open];
    } else if (_button.tag == 4){
        [httpManager cardCompanyList];
    } else if (_button.tag == 5){
        [httpManager downloadLogoImage];
    }
}

-(IBAction)pushedCreditButton:(id)sender{
    _indicator.hidden = NO;
    [_indicator startAnimating];
    _button.enabled = NO;
    _buttonCredit.enabled = NO;
    _isCreditExcute = YES;
    HttpRequestManager* httpManager = [HttpRequestManager sharedManager];
    if (_buttonCredit.tag == 0) {
        [httpManager creditTradeStart];
    } else if (_buttonCredit.tag == 1) {
        [httpManager creditReading];
    }
}

-(void) completedHttpRequestNotification:(NSNotification*) notification{
    NSError* error = notification.userInfo[kHttpRequestCompletedErrorUserInfoName];
    NSLog(@"error = %@",error);
    if (error) {
        _textView.text = error.localizedDescription;
    } else {
        NSData* data = notification.userInfo[kHttpRequestCompletedDataUserInfoName];
        NSString* str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (_isCreditExcute) {
            if (_buttonCredit.tag == 0) {
                NSDictionary* result = [self responseResultData:data];
                if ([result[@"resultCode"] isEqualToString:@"0"]) {
                    [_buttonCredit setTitle:@"クレジット読取" forState:UIControlStateNormal];
                    _buttonCredit.tag = 1;
                }
            }
        } else {
            KeyChainManager* keyChainManager = [KeyChainManager sharedManager];
            if (_button.tag == 0) {
                NSDictionary* normalObj = [self responsedData:data reslutObjectForKey:@"normalObject"];
                NSString* uniqueId = normalObj[@"unique_id"];
                if (uniqueId) {
                    if (![uniqueId isEqualToString:_keyChainInformation.uniqueId]) {
                        _keyChainInformation.certificationData = nil;
                    }
                    _keyChainInformation.uniqueId = uniqueId;
                    keyChainManager.information = _keyChainInformation;
                    [_button setTitle:@"証明書ダウンロード" forState:UIControlStateNormal];
                    _button.tag = 1;
                }
            } else if (_button.tag == 1){
                _keyChainInformation.certificationData = [NSData dataWithData:data];
                keyChainManager.information = _keyChainInformation;
                [_button setTitle:@"アクティベート結果" forState:UIControlStateNormal];
                _button.tag = 2;
            } else if (_button.tag == 2){
                NSDictionary* result = [self responseResultData:data];
                if ([result[@"resultCode"] isEqualToNumber:@(0)]) {
                    _button.tag = 3;
                }
            } else if (_button.tag == 3){
                NSDictionary* sessionObj = [self responsedData:data reslutObjectForKey:@"sessionObject"];
                if ([sessionObj[@"resultCode"] isEqualToNumber:@(0)]) {
                    _button.tag = 4;
                }
            } else if (_button.tag == 5){
                UIImage* image = [UIImage imageWithData:data];
                if (image) {
                    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
                    imageView.frame = CGRectMake(50, 50, 200, 100);
                    [self.view addSubview:imageView];
                }
            }
        }
        
        if (str) {
            _textView.text = str;
        } else {
            _textView.text = [data description];
        }
    }
    NSLog(@"%@",_textView.text);
    _button.enabled = YES;
    _buttonCredit.enabled = YES;
    _textView.textColor = [UIColor colorWithRed:0.0 green:0.4 blue:0.0 alpha:1.0];
    [_indicator stopAnimating];
    _indicator.hidden = YES;
}

@end
