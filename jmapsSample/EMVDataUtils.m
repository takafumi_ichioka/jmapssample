//
//  EMVDataUtils.m
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "EMVDataUtils.h"

@implementation EMVDataUtils

+(NSString*) stringValueWithData:(NSData*) data{
    NSString* stringValue = data.description;
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@"<" withString:@""];
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@">" withString:@""];
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@" " withString:@""];
    return stringValue;
}

+(NSData*) createEmvCommandData:(NSData*) commandData
                      paramData:(NSData*) paramData
                    optinalData:(NSData*) optionalData
                        endData:(NSData *)endData{
    if (!commandData || !paramData) {
        return nil;
    }
    NSMutableData *emvData = commandData.mutableCopy;
    [emvData appendData:paramData];
    NSData* addingOptionalData = [self addingLengthData:optionalData];
    if (addingOptionalData) {
        [emvData appendData:addingOptionalData];
    }
    if (endData && endData.length > 0) {
        [emvData appendData:endData];
    }
    return emvData;
}

+(NSData*) searchTagData:(NSData *)tagData withData:(NSData *)data resultData:(NSData **)resultData{
    if (!data || !tagData || !resultData) {
        *resultData = nil;
        return data;
    }
    NSUInteger dataLength = data.length;
    if (tagData.length > dataLength) {
        *resultData = nil;
        return data;
    }
    NSRange dataRange = [data rangeOfData:tagData
                                  options:NSDataSearchAnchored
                                    range:NSMakeRange(0, dataLength - tagData.length - 1)];
    if (dataRange.location == NSNotFound) {
        *resultData = nil;
        return data;
    }
    NSUInteger searchDataLoc = dataRange.location + dataRange.length;
    NSUInteger searchDataLength = dataLength - searchDataLoc;
    if (searchDataLength == 0) {
        *resultData = nil;
        return data;
    }
    NSData* searchData = [data subdataWithRange:NSMakeRange(searchDataLoc, searchDataLength)];
    NSData* lengthData = [searchData subdataWithRange:NSMakeRange(0, 1)];
    unsigned int len = [self lengthValueWithData:lengthData];
    *resultData = [searchData subdataWithRange:NSMakeRange(1, len)];
    NSUInteger dataLoc = searchDataLoc + 1 + len;
    NSUInteger savedDataLength = dataLength;
    dataLength = dataLength - dataLoc;
    if (dataLength > 0) {
        data = [data subdataWithRange:NSMakeRange(dataLoc, dataLength)];
    } else {
        dataLoc = searchDataLoc + 1;
        dataLength = savedDataLength - dataLoc;
        data = [data subdataWithRange:NSMakeRange(dataLoc, dataLength)];
    }
    return data;
}

+(unsigned int) lengthValueWithData:(NSData*) data{
    NSString* stringValue = [self stringValueWithData:data];
    unsigned int valueLength;
    [[NSScanner scannerWithString:stringValue] scanHexInt:&valueLength];
    return valueLength;
}

+(NSData*) addingLengthData:(NSData*) data{
    if (!data || data.length == 0) {
        return nil;
    }
    NSMutableData* addingLengthData = [NSMutableData data];
    int dataLength = (int)data.length;
    [addingLengthData appendBytes:&dataLength length:1];
    [addingLengthData appendData:data];
    return addingLengthData;
}

@end
