//
//  IncredistManager.h
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSIncredistLib.h"
#import "EMVDataUtils.h"
#import "NSDictionary+ICCBinaryList.h"

typedef void(^IncredistCompletedResponseHandler)(FSIncredistLib* deveice,BOOL connect);
typedef void(^IncredistCompletedEMVCommandHandler)(NSData* metaData,NSData* sw1Data,NSData* sw2Data);
typedef void(^IncredistErrorHandler)(NSString* errorHandler);

@interface IncredistManager : NSObject<FSIncredistLibDelegate>

+(IncredistManager*) sharedManager;

@property(nonatomic,copy) IncredistCompletedResponseHandler completedResponseHandler;
@property(nonatomic,copy) IncredistCompletedEMVCommandHandler completedEMVCommandHandler;
@property(nonatomic,copy) IncredistErrorHandler             responseErrorHandler;
@property(nonatomic,readonly) NSDictionary* readIccBinaryList;

-(void) resetAIDList;

-(void) connectDevice;
-(void) disConnectDevive;

-(void) emvOpen;
-(void) selectFileName:(NSData*) flieName;
-(BOOL) nextSelectAID;
-(void) readRecordByNumber:(int) recordNumber sfiNumber:(int) sfiNumber;
-(void) emvClose;
-(void) answerToReset;


@end
