//
//  EMVDataUtils.h
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMVDataUtils : NSObject

+(NSString*) stringValueWithData:(NSData*) data;
+(NSData*) createEmvCommandData:(NSData*) commandData
                      paramData:(NSData*) paramData
                    optinalData:(NSData*) optionalData
                        endData:(NSData*) endData;
+(NSData*) searchTagData:(NSData*) tagData withData:(NSData*) data resultData:(NSData**) resultData;

+(unsigned int) lengthValueWithData:(NSData*) data;

@end
