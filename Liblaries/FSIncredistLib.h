//
//  FSIncredistLib.h
//
//  Created by J.Suzuki on 2012/12/05.
//  Copyright 2012 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
//
#import <Foundation/Foundation.h>

#define FSEMVClass0X                    0x00
#define FSEMVClassAX                    0xA0



// 接続モード
typedef enum {
	FSConnectionModeNon,									// 未接続
	FSConnectionModeDock				= 1,				// DockConnector接続
	FSConnectionModeBLE,									// Bluetooth接続
} FSConnectionMode;

// 読み込みカード種別
typedef enum {
	FSMagneticCardTypeNon,									//
	FSMagneticCardCredit				= 1,				// クレジットカード
	FSMagneticCardTypeJIS2,									// JIS2規格カード
	FSMagneticCardUnknown,									// 不明カード
} FSMagneticCardType;

// FeliCa System Code
typedef enum {
	FSFelicaSystemCodeAny				= 0xFFFF,			// ANY (ワイルドカード)
	FSFelicaSystemCodeCommon			= 0xFE00,			// Common Area
	FSFelicaSystemCodeCyberne			= 0x0003,			// Cybernetics Area
	FSFelicaSystemCodeEdy				= 0xFE00,			// Edy (同共通領域)
	FSFelicaSystemCodeSFCard			= 0x0003,			// SFCard(Suica, Pasmo等) (同サイバネ領域)
	FSFelicaSystemCodeNon				= 0x0000,			// 該当なし
} FSFelicaSystemCode;

// 暗号化方法
typedef enum {
	FSCipherMethodDUKTPTDES				= 1,				// DUKTP TDES
	FSCipherMethodDUKTPAES				= 2,				// DUKTP AES
	FSCipherMethodFixedTDES				= 3,				// Fixed key TDES
	FSCipherMethodFixedAES128			= 4,				// Fixed key AES128
	FSCipherMethodFixedAES256			= 5,				// Fixed key AES256
} FSCipherMethod;

// ブロック暗号化モード
typedef enum {
	FSBlockCipherModeECB				= 1,				// 暗号ブックモード
	FSBlockCipherModeCBC				= 2,				// 暗号文ブロック連鎖モード
} FSBlockCipherMode;

// Variant constants for transaction keys
typedef enum {
	FSDSConstantPINEncryption			= 1,				// PIN Encryption
	FSDSConstantMessageAuthRequest		= 2,				// Message Authentication, request or both ways
	FSDSConstantMessageAuthResponse		= 3,				// Message Authentication, response
	FSDSConstantDataEncryptionRequest	= 4,				// Data Encryption, request or both ways
	FSDSConstantDataEncryptionResponse	= 5,				// Data Encryption, response
} FSDSConstant;

// パディング方式
typedef enum {
	FSPaddingModeFixedData				= 0,				// fixed data
	FSPaddingModePKCS5					= 1,				// PKCS#5 Padding
} FSPaddingMode;

// 暗号化設定構造体
typedef struct {
	int keyNumber;											// キー番号
	FSCipherMethod cipherMethod;							// 暗号化方法
	FSBlockCipherMode cipherMode;							// ブロック暗号化モード
	FSDSConstant keyConstant;								// Variant constants for transaction keys
    FSPaddingMode paddingMode;								// パディング方式
	unsigned char paddingValue;								// パディングデータ
	BOOL pin;												// PIN入力有無
} EncryptionMode;



@protocol FSIncredistLibDelegate;

#pragma mark -
#pragma mark FSIncredistLib Interface
@interface FSIncredistLib : NSObject {
}

#pragma mark Property Declaration
@property (weak, nonatomic) id<FSIncredistLibDelegate> delegate;
@property (readonly) BOOL connected;
@property (readonly) FSConnectionMode connectionMode;
@property (readonly) FSConnectionMode lastConnectionMode;
@property int scanMode;
@property NSTimeInterval emvTimeoutInterval;
@property (nonatomic, strong) NSData *apduResponse;
@property UInt16 apduStatusWord;                                    // APDU Response内のSW1,SW2 (以下、一例)
@property UInt16 apduSelectingEFID;
@property BOOL isUpdatedMSISDN;
@property (nonatomic, strong) volatile NSData *nfcid;
@property (nonatomic, strong) NSData *nfcResponse;
@property volatile BOOL isNfcPolling;
@property UInt16 nfcErrCode;
@property UInt16 nfcStatusWord;
@property UInt8 nfcLockStatus;
@property (nonatomic, strong) volatile NSData *flcidm;
@property (nonatomic, strong) volatile NSData *flcpmm;
@property (nonatomic, strong) NSData *flcResponse;
@property volatile BOOL isFlcPolling;
@property UInt16 flcErrCode;


+ (FSIncredistLib *)sharedInstance;
- (BOOL)connect;
- (int)connectToRegisteredDevices:(NSString *)bluetoothAddress;
- (BOOL)connectWithTimeout:(int)timeout;
- (int)connectToRegisteredDevices:(NSString *)bluetoothAddress withTimeout:(int)timeout;
- (BOOL)connectToDock;
- (int)connectToBluetooth;
- (void)disconnect;
- (void)stop;

- (void)getDeviceUDID;
- (void)getDeviceInformation;
- (void)checkBatteryLevel;
- (void)getFirmwareVersion;
- (void)testKeyPad;
- (void)getRealTime;
- (BOOL)setRealTime:(NSString *)realTime;

- (void)getKSN;
- (BOOL)setSerialNumber:(NSString *)serialNumber;

- (BOOL)setDisplayString:(NSString *)displayString;
- (void)getDisplayString;

- (void)getBluetoothAddress;
- (void)registerBluetoothAddressToDevice;
- (NSString *)readBluetoothAddressToDevice;
- (void)removeBluetoothAddressFromDevice;
- (void)writeBluetoothAddressToDevice:(NSString *)bluetoothAddress;
- (void)setBluetoothBuffersize:(int)size;

- (void)transitFirmwareUpdateMode;
- (void)updateFirmware:(NSString *)filePath;
- (void)sendStartFirmwareUpdateCommand;

- (void)scanBarcode;
- (void)scanBarcodeWithType;
- (void)stopBarcodeScan;

- (void)setEncryptionMode:(EncryptionMode)mode;
- (void)getEncryptionMode;
- (void)scanMagneticCard;
- (void)scanMagneticCardWithEncryptionMode;
- (void)stopMagneticCardScan;

- (void)scanPayeasy;
- (void)scanPayeasyWithPin;
- (void)stopPayeasyScan;

- (void)scanEMV;
- (void)scanEMVWithPin;
- (void)stopEMV;

- (void)felicaInitialize;
- (void)felicaPolling:(UInt16)systemCode;
- (void)felicaPolling2:(UInt16)systemCode;
- (void)felicaRequestSystemCode;
- (void)felicaRequestResponse;
- (void)felicaLiteRead:(UInt8)block;
- (void)felicaLiteWrite:(UInt8)block data:(UInt8 *)buf;
- (void)felicaLiteMultiRead:(UInt8)count number:(UInt8 [])block;
- (void)felicaLiteMultiWrite:(UInt8)count number:(UInt8 [])block data:(UInt8 [])data;
- (void)felicaSendCommand:(NSData *)command;
- (void)felicaSetGainParameters:(NSData *)parameters;
- (void)felicaClose;

- (void)emvOpen;
- (void)emvOpenEx:(int)cla;
- (void)emvSendCommand:(NSData *)command;
- (void)emvClose;

// USIM Writer&Checker用メソッド
- (UInt16)emvSendApduCommand:(NSData *)apdu;
- (UInt16)emvSelectEFID:(UInt16)efid;
- (UInt16)emvSelectAID:(NSData *)aid;
- (NSData *)emvGetResponse;
- (NSData *)emvReadRecord;
- (NSData *)emvReadRecord:(UInt8)recordNumber Mode:(UInt8)mode Length:(UInt8)length;
- (NSData *)emvReadBinary;
- (NSData *)emvReadBinaryWithLength:(UInt8)length;
- (NSString *)emvReadICCID;
- (NSString *)emvReadMSISDN;
- (UInt16)emvVerifyPIN:(NSString *)pin;
- (UInt16)emvChangePIN:(NSString *)oldPin NewPIN:(NSString *)newPin;
- (UInt16)emvDisablePIN:(NSString *)pin;
- (UInt16)emvEnablePIN:(NSString *)pin;
- (UInt16)emvUnblockPIN:(NSString *)pin PUK:(NSString *)puk;
- (UInt16)emvDetailCheck;
- (UInt16)emvNfcSelectLockApplet;
- (UInt16)emvNfcReset:(NSString *)puk;
- (UInt16)emvUpdateMSISDN:(NSData *)userData Header:(NSData *)header CodingScheme:(UInt8)scheme ProtocolIdentifier:(UInt8)identifier;

// NFCアクセス用メソッド
- (void)nfcOpen;
- (void)nfcPolling:(UInt8)target;
- (void)nfcPollingStart:(UInt8)target;
- (void)nfcPollingStop;
- (void)nfcSendCommand:(NSData *)data;
- (UInt16)nfcActivate;
- (UInt16)nfcSelectLockApplet;
- (UInt16)nfcRegisterWrite:(UInt8)addr Data:(NSData *)data;
- (UInt16)nfcRegisterWriteChar:(UInt8)addr Data:(UInt8)data;
- (UInt16)nfcRegisterRead:(UInt8)addr Length:(UInt8)len;
- (int)nfcRegisterReadChar:(UInt8)addr;
- (void)nfcClose;

- (void)searchDeviceIDFormKeychain;

- (NSString *)getApplicationVersion;

@end


#pragma mark -
#pragma mark FSIncredistLib Delegate
@protocol FSIncredistLibDelegate <NSObject>
@required
- (void)incredistDidConnect:(FSIncredistLib *)device;
- (void)incredistDidDisconnect:(FSIncredistLib *)device;
- (void)connectError:(FSIncredistLib *)device error:(NSString *)errorMessage;
- (void)scannerError:(FSIncredistLib *)device error:(NSString *)errorMessage;
//- (void)incredistDidUnsupportedBLE;

@optional
- (void)incredistDidActive;
- (void)didUpdateBLEState:(BOOL)enabled;
- (void)connectTimeOut;
- (void)didEntryDeviceID:(NSString *)DeviceID;
- (void)didGetDeviceUDID:(NSString *)UDID;
- (void)didGetDeviceInformation:(NSString *)infor;
- (void)didGetBatteryLevel:(NSString *)level;
- (void)didGetFirmwareVersion:(NSString *)version;
- (void)didSetTime;
- (void)didGetRealTime:(NSString *)realTime;
- (void)didGetBluetoothAddress:(NSString *)bluetoothAddress;
- (void)didTransitFirmwareUpdateMode:(BOOL)result error:(NSString *)errorMessage;

- (void)didScanBarcode:(NSString *)barcode;

- (void)didSetEncryptionMode;
- (void)didGetEncryptionMode:(EncryptionMode)mode;
- (void)didScanMagneticCard:(FSMagneticCardType)cardType ksn:(NSString *)ksn track:(NSArray *)track
                     cardNo:(NSString *)cardNo expirationDate:(NSString *)date;

- (void)didScanPayeasyCardSwipe;
- (void)didInputPayeasyPin:(FSMagneticCardType)cardType ksn:(NSString *)ksn track:(NSArray *)track
					cardNo:(NSString *)cardNo expirationDate:(NSString *)date;

- (void)didScanICCard;
- (void)didInputICCardPin:(FSMagneticCardType)cardType ksn:(NSString *)ksn track:(NSArray *)track
				   cardNo:(NSString *)cardNo expirationDate:(NSString *)date;

- (void)didReceiveFelicaResponse:(NSData *)data;

- (void)didBootFirmwareUpdateMode;
- (void)didWriteFirmware:(BOOL)finished rate:(double)rate;
- (void)firmwareTimeout;

- (void)didGetKSN:(NSData *)ksn;
- (void)didSetSerialNumber;

- (void)didSetDisplayString:(NSString *)displayString;
- (void)didGetDisplayString:(NSString *)displayString;

- (void)didGetEMVICCOpenResponse:(NSData *)emvResponse;
- (void)didGetEMVICCSendCommandResponse:(NSData *)emvResponse;
- (void)didGetEMVICCCloseResponse;

- (void)didGetNFCPollingResponse:(NSData *)data;
- (void)didGetNFCSendCommandResponse:(NSData *)data;
- (void)didDiscoverNFCDevice:(NSData *)nfcid;

- (void)didGetFlcPollingResponse:(NSData *)data;
- (void)didGetFlcSendCommandResponse:(NSData *)data;
- (void)didDiscoverFlcDevice:(NSData *)flcidm pmm:(NSData *)flcpmm;

@end
